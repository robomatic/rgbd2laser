#ifndef IMAGEUTILS_H
#define IMAGEUTILS_H

#include <iostream>

#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>

#include <opencv2/core/core.hpp>

using namespace std;
using namespace cv;

class ImageUtils
{
    public:
      static Mat convertRosImgToCvMat(const sensor_msgs::ImageConstPtr& msg);
};

#endif //IMAGEUTILS_H
