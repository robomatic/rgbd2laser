#ifndef MATHSOLVER_H
#define MATHSOLVER_H

#include <ros/ros.h>
#include <sensor_msgs/CameraInfo.h>

#include <opencv2/core/core.hpp>

using namespace std;
using namespace cv;

class MathSolver
{
    public:
        static void transformToPointCS(vector<Point3f> points, vector<Point3f> &newPoints);
        static void calculatePlane(vector<Point3f> points, Vec4f &coefficients);
        static void calculateCircleCenter(vector<Point3f> points, Point3f &center);
        static float calculateDistance(Point3f point, Point3f center);
        static void getTranslationVector(Point3f from, Point3f to, Vec3f &translation);
        static void calculateTranslation(vector<vector<Point3f> > points, float &radius, Vec3f &translation, float range = 0.3f);
        static void get3DPointFromDepthImage(Mat image, sensor_msgs::CameraInfo cameraInfo, Point2d imagePoint, Point3f &point);
        static void calculateYawAngle(vector<vector<Point3f> > points, Vec3f translation, float &yaw);
        static void calculateAngle(Point3f a1, Point3f a2, Point3f b1, Point3f b2, float angle);

        static const int FX;
        static const int FY;
        static const int CX;
        static const int CY;

    private:
        static int findCentersInRange(vector<Point3f> points, Point3f point, float range);
        static int findIndexOfMax(vector<int> numbers);
};

#endif //MATHSOLVER_H
