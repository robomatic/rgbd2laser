#ifndef CAMERA_CALIBRATION_H
#define CAMERA_CALIBRATION_H

// std
#include <string>
#include <opencv2/core/core.hpp>

// ros
#include <ros/ros.h>
#include <sensor_msgs/LaserScan.h>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/Image.h>

// eigen
#include <Eigen/Eigen>

// pcl
#include <pcl/visualization/pcl_visualizer.h>

#include <message_filters/subscriber.h>
#include <message_filters/time_synchronizer.h>
#include <message_filters/sync_policies/exact_time.h>
#include <sensor_msgs/CameraInfo.h>

using namespace std;

class CameraCalibration
{
public:

    //============================================================================
    //============================= PUBLIC METHODS ===============================
    //============================================================================

    /** \brief Constructor. */
    CameraCalibration(ros::NodeHandle& n);

    /**
     * @brief publish       Method for publishing the calibrated transformation.
     */
    void publish();

protected:

    //============================================================================
    //=============================== TYPEDEFS ===================================
    //============================================================================

    /**
     * @brief Visualizer        PCLVisualizer typedef.
     */
    typedef pcl::visualization::PCLVisualizer Visualizer;

    /**
     * @brief VisualizerPtr     Boost pointer for PCLVisualizer.
     */
    typedef boost::shared_ptr<Visualizer> VisualizerPtr;

    /**
     * @brief PointCloudXYZ     Pointcloud typedef.
     */
    typedef pcl::PointCloud<pcl::PointXYZ> PointCloudXYZ;

    /**
     * @brief PointCloudXYZRGB  Pointcloud typedef.
     */
    typedef pcl::PointCloud<pcl::PointXYZRGB> PointCloudXYZRGB;

    //============================================================================
    //============================ ROS SUBSCRIBERS ===============================
    //============================================================================

    /**
     * @brief sub_cloud                 Subscriber of the point cloud.
     */
    ros::Subscriber sub_cloud;

    /**
     * @brief sub_laser_scan            Subscriber of the laser scan.
     */
    ros::Subscriber sub_laser_scan;

    //============================================================================
    //============================== ROS PARAMS ==================================
    //============================================================================

    /**
     * @brief robot_name                Name of the robot we calibrate sensor for.
     */
    string robot_name;

    /**
     * @brief sensor_name               Name of the sensor passed as ros param.
     */
    string sensor_name;

    //============================================================================
    //=============================== VARIABLES ==================================
    //============================================================================

    /**
     * @brief laser_to_base_matrix      Transformation matrix from /batman/base_laser_link to
     *                                  /batman/base_link frame.
     */
    Eigen::Affine3d laser_to_base_matrix;

    /**
     * @brief state                     State of actual calibration progress.
     */
    int state;

    /**
     * @brief viewer                    PCLVisualizer object.
     */
    VisualizerPtr viewer;

    /**
     * @brief viewer_cloud              Point cloud to visualize in @viewer.
     */
    PointCloudXYZRGB::Ptr viewer_cloud;

    /**
     * @brief viewer_inliers            Segmented inliers.
     */
    pcl::PointIndices::Ptr viewer_inliers;
    pcl::ModelCoefficients::Ptr viewer_coeff;

    /**
     * @brief sensor_pitch              The pitch angle of the sensor.
     */
    float sensor_pitch;

    /**
     * @brief sensor_yaw                The yaw angle of the sensor.
     */
    float sensor_yaw;

    /**
     * @brief sensor_x                  X translation of the sensor.
     */
    float sensor_x;

    /**
     * @brief sensor_y                  Y translation of the sensor.
     */
    float sensor_y;

    /**
     * @brief sensor_z                  Z translation of the sensor.
     */
    float sensor_z;

    /**
     * @brief laser_index_min           Minimum laser index for wall determining.
     */
    int laser_index_min;

    /**
     * @brief laser_index_max           Maximum laser index for wall determining.
     */
    int laser_index_max;

    /**
     * @brief laser_a_1                 The 'a' coefficient in linear equation of detected wall
     *                                  in laser scan data in the first iteration.
     */
    float laser_a_1;

    /**
     * @brief laser_b_1                 The 'b' coefficient in linear equation of detected wall
     *                                  in laser scan data in the first iteration.
     */
    float laser_b_1;

    /**
     * @brief cloud_a_1                 The 'a' coefficient in linear equation of detected wall
     *                                  in point cloud in the first iteration.
     */
    float cloud_a_1;

    /**
     * @brief cloud_b_1                 The 'b' coefficient in linear equation of detected wall
     *                                  in laser scan data in the first iteration.
     */
    float cloud_b_1;

    /**
     * @brief laser_a_2                 The 'a' coefficient in linear equation of detected wall
     *                                  in laser scan data in the second iteration.
     */
    float laser_a_2;

    /**
     * @brief laser_b_2                 The 'b' coefficient in linear equation of detected wall
     *                                  in laser scan data in the second iteration.
     */
    float laser_b_2;

    /**
     * @brief cloud_a_2                 The 'a' coefficient in linear equation of detected wall
     *                                  in point cloud in the second iteration.
     */
    float cloud_a_2;

    /**
     * @brief cloud_b_2                 The 'b' coefficient in linear equation of detected wall
     *                                  in laser scan data in the second iteration.
     */
    float cloud_b_2;

    //============================================================================
    //============================= ROS CALLBACKS ================================
    //============================================================================

    /**
     * @brief processCloud              ROS callback.
     * @param cloud                     Cloud received from ASUS Xtion.
     */
    void processCloud(const sensor_msgs::PointCloud2ConstPtr &cloud);

    /**
     * @brief processLaserScan          ROS callback.
     * @param scan                      Data from laser scanner.
     */
    void processLaserScan(const sensor_msgs::LaserScanConstPtr &scan);

    //============================================================================
    //============================= PCL CALLBACKS ================================
    //============================================================================

    /**
     * @brief keyboardViewerEvent       PCLVisualizer keyboard event callback.
     * @param event                     PCL keyboard event.
     * @param viewer_void               Pointer to the viewer instance.
     */
    void keyboardViewerEvent(const pcl::visualization::KeyboardEvent &event, void* viewer_void);

    //============================================================================
    //================================ METHODS ===================================
    //============================================================================

    /**
     * @brief determinePitchAngle       Method for determining pitch angle of camera.
     * @param cloud                     Cloud from sensor.
     */
    void determinePitchAngle(const sensor_msgs::PointCloud2ConstPtr &cloud);

    /**
     * @brief determineYawXYAngle       Method for determining yaw angle (first call) and x and y translation
     *                                  (second call).
     * @param cloud                     Cloud from sensor.
     */
    void determineYawXYAngle(const sensor_msgs::PointCloud2ConstPtr &cloud);

    /**
     * @brief findWallOnLaserScan       Method for determining the wall for calibration with RGB-D sensor.
     * @param scan                      Data from laser scanner.
     */
    void findWallOnLaserScan(const sensor_msgs::LaserScanConstPtr &scan);

    void findBiggestPlane();
    void removeBiggestPlane();

    void visualize(const sensor_msgs::PointCloud2ConstPtr &cloud);
    void visualize(const sensor_msgs::LaserScanConstPtr &scan);

    void saveTransform();
};

#endif //CAMERA_CALIBRATION_H
