#ifndef TRACKER_H
#define TRACKER_H

#include <iostream>

#include <ros/ros.h>

#include <opencv2/core/core.hpp>
#include "opencv2/features2d/features2d.hpp"
//#include "opencv2/imgcodecs/imgcodecs.hpp"
#include "opencv2/highgui/highgui.hpp"

#include <mathsolver.h>

using namespace std;
using namespace cv;

class Tracker
{
    public:
        //OpenCV <= 3.0 API
        Tracker();
        //OpenCV 3.2 API
        Tracker(Ptr<Feature2D> detector);
        void setFirstFrame(const Mat frame);
        void process(const Mat frame);
        void get3DPoints(Mat depth1, Mat depth2,
                sensor_msgs::CameraInfo cameraInfo,
                vector<vector<Point3f> > &points);
        void get3DPoints(Mat depth1, Mat depth2, Mat depth3,
                sensor_msgs::CameraInfo cameraInfo,
                vector<vector<Point3f> > &points);

    private:
        const int MAX_ITERATION = 2;
        const int OPENCV_API_UP_TO_30 = 0;
        const int OPENCV_API_32 = 1;
        int iteration;
        int openCvApi;

        //OpenCV <= 3.0 API
        ORB orb;
        //OpenCV 3.2 API
        Ptr<Feature2D> detector;
        FlannBasedMatcher matcher;
        Mat firstFrame;
        Mat firstDescriptors;
        vector<KeyPoint> firstKeypoints;
        vector<KeyPoint> processedKeypoints1;
        vector<KeyPoint> processedKeypoints2;
        vector<DMatch> matches1;
        vector<DMatch> matches2;

        void clearData();
};

#endif //TRACKER_H
