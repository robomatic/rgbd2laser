#ifndef RGBD2LASER_H
#define RGBD2LASER_H

#include <math.h>                           //PI definition
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>

#include <ros/ros.h>
#include <nodelet/nodelet.h>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/LaserScan.h>

#include <gpu_utils.h>
#include <transform_matrix.h>

#include <message_filters/subscriber.h>
#include <message_filters/time_synchronizer.h>
#include <message_filters/sync_policies/approximate_time.h>

#include <boost/chrono.hpp>

class RGBD2Laser : public nodelet::Nodelet
{
public:

    //====================================================
    //===================== TYPEDEFS =====================
    //====================================================

    /** \brief Message filters with exact time policy. */
    typedef message_filters::sync_policies::ApproximateTime<sensor_msgs::PointCloud2,
                                                            sensor_msgs::PointCloud2> MySyncPolicy;

    // ================================
    // ======== PUBLIC METHODS ========
    // ================================

    /** \brief Constructor. */
//    RGBD2Laser(ros::NodeHandle n);

    virtual void onInit();

    /** \brief Constructor. */
    ~RGBD2Laser();

    /** \brief Publish. */
    void publish();

protected:

    //===================================
    //========= PUBLIC VARIABLES ========
    //===================================

    /** \brief Camera1 depth image subscriber. */
    message_filters::Subscriber<sensor_msgs::PointCloud2> *sub_sensor_depth_1;

    /** \brief Camera2 depth image subscriber. */
    message_filters::Subscriber<sensor_msgs::PointCloud2> *sub_sensor_depth_2;

    /** \brief Cameras messages synchronization. */
    message_filters::Synchronizer<MySyncPolicy> *sync;

    // ==================================
    // ======== ROS PUBLISHERS ==========
    // ==================================

    ros::Publisher pub_cloud_merged;
    ros::Publisher pub_cloud_floor;
    ros::Publisher pub_cloud_positive;
    ros::Publisher pub_cloud_negative;
    ros::Publisher pub_laser_scan;

    // ==================================
    // ========= ROS SUBSCRIBERS ========
    // ==================================

    ros::Subscriber sub_cloud_1;
    ros::Subscriber sub_cloud_2;

    /** \brief Sensor cloud received callback */
    void sensorCb( const sensor_msgs::PointCloud2ConstPtr &cloud_1,
                   const sensor_msgs::PointCloud2ConstPtr &cloud_2);

    // ==================================
    // ========== ROS MESSAGES ==========
    // ==================================

    sensor_msgs::LaserScan laser_scan;
    sensor_msgs::PointCloud2 *cloud_floor;
    sensor_msgs::PointCloud2 *cloud_positive;
    sensor_msgs::PointCloud2 *cloud_negative;

    // ==================================
    // ======== ROS CALLBACKS ===========
    // ==================================

    void processCloud1(const sensor_msgs::PointCloud2ConstPtr &cloud);
    void processCloud2(const sensor_msgs::PointCloud2ConstPtr &cloud);

    sensor_msgs::PointCloud2ConstPtr cloud_1;
    sensor_msgs::PointCloud2ConstPtr cloud_2;

    // ==================================
    // ===== TRANSFORMATION MATRIX ======
    // ==================================

    TransformMatrix transformation_matrix1;
    TransformMatrix transformation_matrix2;

    // ==================================
    // ============== TEMP ==============
    // ==================================


    bool sensor1, sensor2, debug, sensors_sync, verbose;

    float cell_size;
    float sensor_radius;
    float tolerance;

    GPUUtils *gpu;
};

#endif //RGBD2LASER_H
