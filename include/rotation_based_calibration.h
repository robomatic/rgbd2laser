#ifndef ROTATION_BASED_CALIBRATION_H
#define ROTATION_BASED_CALIBRATION_H

// std
#include <string>
#include <iostream>

//OpenCV
#include <opencv2/core/core.hpp>

// ros
#include <ros/ros.h>

// eigen
#include <Eigen/Eigen>

//image processing
#include <imageutils.h>

//math calculations
#include <mathsolver.h>

//tracking keypoints
#include <tracker.h>

using namespace std;

class RotationBasedCalibration
{
    public:

        //============================================================================
        //================================= TYPEDEFS =================================
        //============================================================================

        /** \brief RGBDFrame typedef as a pair of two images: color and depth. */
        typedef std::pair<cv::Mat, cv::Mat> RGBDFrame;

        /**
         *  \brief  RGBD2FrameWithAngle typedef as RGBDFrame and angle between some
         *          arbitrary direction (you could use yaw angle of first RGBDFrame
         *          as reference.
         */
        typedef std::pair<RGBDFrame, float> RGBDFrameWithAngle;

        //============================================================================
        //============================= MAIN METHODS =================================
        //============================================================================

        /**
         * @brief RotationBasedCalibration      Default constructor.
         */
        RotationBasedCalibration();

        /**
         * @brief addFrame                      Add frame to be processed.
         * @param[in] image_bgr                 BGR image as opencv mat.
         * @param[in] image_depth               Depth image as opencv mat.
         * @return                              True if succeed.
         */
        bool addFrame(const cv::Mat &image_bgr, const cv::Mat &image_depth, const float yaw_angle);


        /**
         * @brief estimateTransformationMatrix  Estimate transformation matrix based on
         *                                      stored frames. This method clears frames
         *                                      list after the estimation.
         * @param[out] transformation_matrix    Estimated transformation matrix.
         * @return                              True if succeed.
         */
        bool estimateTransformationMatrix(Eigen::Affine3d &transformation_matrix);

        /**
         * @brief processRotation               Calculates average values for a radius
         *                                      and a translation vector. STAGE-1 of
         *                                      a estimation process.
         * @return                              True if succeed;
         */
        bool processRotation();

        /**
         * @brief processMovement               Calculates average values for YAW angle.
         *                                      STAGE-2 of a estimation process.
         * @return                              True if succeed.
         */
        bool processMovement();

        //============================================================================
        //============================ SETTERS/GETTERS ===============================
        //============================================================================

        /**
         * @brief getFramesCount                Get number of RGBDFrames stored so far.
         * @return                              As above.
         */
        inline int getFramesCount()
        {
            return frames.size();
        }

        /**
         * @brief setCameraParameters           Set camera parameters.
         * @param cameraInfo                    Camera parameters.
         */
        void setCameraParameters(sensor_msgs::CameraInfo cameraParams);

    private:

        //============================================================================
        //=============================== VARIABLES ==================================
        //============================================================================

        /** \brief Observed frames filtered with the MIN_ANGLE_INCREMENT. */
        std::vector<RGBDFrameWithAngle> frames;

        /** \brief Based information about a camera parameters. */
        sensor_msgs::CameraInfo cameraInfo;

        /**
         * @brief radius                        Distance between the center of a robot's
         *                                      frame and a camera mounting point.
         */
        float radius;
        /**
         * @brief translation                   Translation vector - the center of a
         *                                      robot's frame and a camera mounting point.
         */
        Vec3f translation;

        /**
         * @brief yaw                           YAW angle.
         */
        float yaw;
};

#endif //ROTATION_BASED_CALIBRATION_H
