#include <ros/ros.h>
#include <rgbd2laser.h>

int main(int argc, char** argv)
{
    //Init ros node
    ros::init(argc, argv, "rgbd2laser");

    //Ros variables
    ros::NodeHandle n("~");
    ros::Rate rate(30);

    //RGBD2Laser constructor
    RGBD2Laser rgbd2laser(n);

    ros::spin();
/*
    //Loop
    while(ros::ok())
    {
        //Spin
        ros::spinOnce();

        //Publish
        rgbd2laser.publish();

        //Sleep
        rate.sleep();
    }*/

    return 0;
}
