#include <tracker.h>

Tracker::Tracker()
{
    this->orb = ORB();
    this->openCvApi = OPENCV_API_UP_TO_30;
}

Tracker::Tracker(Ptr<Feature2D> detector)
{
    this->iteration = 0;
    this->detector = detector;
    this->openCvApi = OPENCV_API_32;
}

void Tracker::setFirstFrame(const Mat frame)
{
    firstFrame = frame.clone();
    if (openCvApi == OPENCV_API_32)
        detector->detectAndCompute(firstFrame, noArray(), firstKeypoints, firstDescriptors);
    else
        orb.detectAndCompute (firstFrame, noArray (), firstKeypoints, firstDescriptors);
}

void Tracker::process(const Mat frame)
{
    Mat descriptors;

    iteration++;

    //---------------------------------------------------------------------------------------------
    //--- Detect the keypoints using ORB Detector and calculate descriptors (feature vectors) -----
    //---------------------------------------------------------------------------------------------
    if (openCvApi == OPENCV_API_32)
        detector->detectAndCompute(
                    frame,
                    noArray(),
                    (iteration == 1 ? processedKeypoints1 : processedKeypoints2),
                    descriptors);
    else
        orb.detectAndCompute (
                    frame,
                    noArray (),
                    (iteration == 1 ? processedKeypoints1 : processedKeypoints2),
                    descriptors);

    //---------------------------------------------------------------------------------------------
    //--- Matching descriptor vectors using FLANN matcher -----------------------------------------
    //---------------------------------------------------------------------------------------------
    vector<DMatch> matches;
    if (firstDescriptors.type() != CV_32F)
        firstDescriptors.convertTo(firstDescriptors, CV_32F);
    if (descriptors.type() != CV_32F)
        descriptors.convertTo(descriptors, CV_32F);
    matcher.match(firstDescriptors, descriptors, matches);

    double max_dist = 0;
    double min_dist = 100;

    //---------------------------------------------------------------------------------------------
    //--- Calculation of max and min distances between keypoints ----------------------------------
    //---------------------------------------------------------------------------------------------
    for (int i = 0; i < matches.size(); i++)
    {
        double dist = matches[i].distance;
        if (dist < min_dist)
            min_dist = dist;
        if (dist > max_dist)
            max_dist = dist;
    }

    //---------------------------------------------------------------------------------------------
    //--- Consider only "good" matches (distance is less than 2*min_dist or a small arbitary value
    //--- (0.02) in the event that min_dist is very small) ----------------------------------------
    //---------------------------------------------------------------------------------------------
    for (int i = 0; i < matches.size(); i++)
    {
        if (matches[i].distance <= max(2*min_dist, 0.02))
            iteration == 1 ? matches1.push_back(matches[i]) : matches2.push_back(matches[i]);
    }

//    //---------------------------------------------------------------------------------------------
//    //--- Visualization ---------------------------------------------------------------------------
//    //---------------------------------------------------------------------------------------------
//    //--- Drawing only "good" matches -------------------------------------------------------------
//    Mat imgMatches;
//    drawMatches(
//                firstFrame, firstKeypoints,
//                frame, (iteration == 1 ? processedKeypoints1 : processedKeypoints2),
//                (iteration == 1 ? matches1 : matches2), imgMatches,
//                Scalar::all(-1), Scalar::all(-1),
//                vector<char>(), DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS);

//    //--- Showing detected matches ----------------------------------------------------------------
//    imshow("Good Matches", imgMatches);
//    waitKey(0);
}

void Tracker::get3DPoints(
        Mat depth1, Mat depth2,
        sensor_msgs::CameraInfo cameraInfo,
        vector<vector<Point3f> > &points)
{
    for (int i = 0; i < matches1.size(); i++)
    {
        vector<Point3f> tempPoints;
        Point3f tempPoint;

        //--- First point: x = x0 -------------------------------------------------
        MathSolver::get3DPointFromDepthImage(
                    depth1,
                    cameraInfo,
                    firstKeypoints.at(matches1.at(i).queryIdx).pt,
                    tempPoint);
        tempPoints.push_back(tempPoint);
        //--- Second point: x = x0 + INCx ---------------------------------------
        MathSolver::get3DPointFromDepthImage(
                    depth2,
                    cameraInfo,
                    processedKeypoints1.at(matches1.at(i).trainIdx).pt,
                    tempPoint);
        tempPoints.push_back(tempPoint);

        points.push_back(tempPoints);
    }

    clearData();
}

void Tracker::get3DPoints(
        Mat depth1, Mat depth2, Mat depth3,
        sensor_msgs::CameraInfo cameraInfo,
        vector<vector<Point3f> > &points)
{
    for (int i = 0; i < matches1.size(); i++)
    {
        for (int j = 0; j < matches2.size(); j++)
            if (matches1.at(i).queryIdx == matches2.at(j).queryIdx)
            {
                vector<Point3f> tempPoints;
                Point3f tempPoint;

                //--- First point: alpha = alpha0 -------------------------------------------------
                MathSolver::get3DPointFromDepthImage(
                            depth1,
                            cameraInfo,
                            firstKeypoints.at(matches1.at(i).queryIdx).pt,
                            tempPoint);
                tempPoints.push_back(tempPoint);
                //--- Second point: alpha = alpha0 + INCdeg ---------------------------------------
                MathSolver::get3DPointFromDepthImage(
                            depth2,
                            cameraInfo,
                            processedKeypoints1.at(matches1.at(i).trainIdx).pt,
                            tempPoint);
                tempPoints.push_back(tempPoint);
                //--- Third point: alpha = alpha0 + 2*INCdeg --------------------------------------
                MathSolver::get3DPointFromDepthImage(
                            depth3,
                            cameraInfo,
                            processedKeypoints2.at(matches2.at(j).trainIdx).pt,
                            tempPoint);
                tempPoints.push_back(tempPoint);

                points.push_back(tempPoints);
            }
    }

    clearData();
}

void Tracker::clearData()
{
    firstKeypoints.clear();
    processedKeypoints1.clear();
    processedKeypoints2.clear();

    matches1.clear();
    matches2.clear();

    iteration = 0;
}
