#include <rgbd2laser.h>
#include <pluginlib/class_list_macros.h>
#include <tf/transform_listener.h>
#include <tf/LinearMath/Vector3.h>
#include <tf/LinearMath/Matrix3x3.h>

// watch the capitalization carefully
PLUGINLIB_EXPORT_CLASS(RGBD2Laser, nodelet::Nodelet) 

//RGBD2Laser::RGBD2Laser(ros::NodeHandle n)
void RGBD2Laser::onInit()
{
    NODELET_DEBUG("Initializing rgbd2laser nodelet...");
    
    //==================================================================
    //============================== ROS PARAMS ============================
    //==================================================================

    ros::NodeHandle n("~");

    std::string base_link_frame;
    std::string camera_left_link_frame, camera_right_link_frame;
    std::string camera_left_topic, camera_right_topic;
    std::string output_laser_scan_topic;

    getPrivateNodeHandle().getParam("sensor1", sensor1);
    getPrivateNodeHandle().getParam("sensor2", sensor2);
    getPrivateNodeHandle().param<bool>("sensors_sync", sensors_sync, true);

    getPrivateNodeHandle().param<std::string>("base_link_frame", base_link_frame, "/batman/base_link");
    getPrivateNodeHandle().param<std::string>("camera_left_link_frame", camera_left_link_frame, "camera_left_link");
    getPrivateNodeHandle().param<std::string>("camera_right_link_frame", camera_right_link_frame, "camera_right_link");

    getPrivateNodeHandle().param<std::string>("output_laser_scan_topic", output_laser_scan_topic, "scan");
    getPrivateNodeHandle().param<std::string>("camera_left_topic", camera_left_topic, "/camera_left/depth/points");
    getPrivateNodeHandle().param<std::string>("camera_right_topic", camera_right_topic, "/camera_right/depth/points");

    //==================================================================
    //========================= Sub & Publishers =======================
    //==================================================================

    // GPU
    gpu = new GPUUtils(sensor1, sensor2);

    if (sensors_sync)
    {

        if (!sensor1 || !sensor2)
        {
            ROS_ERROR("In sync mode both sensors should be enabled.");
            std::exit(-1);
        }

        sub_sensor_depth_1 = new message_filters::Subscriber<sensor_msgs::PointCloud2>(n, camera_left_topic, 2);
        sub_sensor_depth_2 = new message_filters::Subscriber<sensor_msgs::PointCloud2>(n, camera_right_topic, 2);

        sync = new message_filters::Synchronizer<MySyncPolicy>(MySyncPolicy(2), *sub_sensor_depth_1, *sub_sensor_depth_2);
        sync->registerCallback(boost::bind(&RGBD2Laser::sensorCb, this, _1, _2));

    } else
    {
        if (sensor1)
        {
            sub_cloud_1 = n.subscribe<sensor_msgs::PointCloud2>(camera_left_topic,1, &RGBD2Laser::processCloud1, this, ros::TransportHints().tcpNoDelay());
            ROS_INFO("Starting rgbd2laser_driver for first sensor.");
        }
        if (sensor2)
        {
            sub_cloud_2 = n.subscribe<sensor_msgs::PointCloud2>(camera_right_topic,1, &RGBD2Laser::processCloud2, this, ros::TransportHints().tcpNoDelay());
            ROS_INFO("Starting rgbd2laser_driver for second sensor.");
        }
        if (!sensor1 && !sensor2)
        {
            ROS_ERROR("No sensors specified - you should enable at least one of sensor1 or sensor2.");
            std::exit(-1);
        }

        ROS_INFO("Waiting for sensors to connect...");
        ros::Rate(0.2).sleep();
        if (!sub_cloud_1.getNumPublishers() && !sub_cloud_2.getNumPublishers())
        {
            ROS_ERROR("There is no primesense sensor connected! Please connect at least one sensor and try again...");
            std::exit(-1);
        }
    }
    ROS_INFO("Sensors connected successfully");

    pub_laser_scan = n.advertise<sensor_msgs::LaserScan>(output_laser_scan_topic, 1);
    laser_scan.header.frame_id = base_link_frame;
    laser_scan.angle_increment = ANGLE_INCREMENT_RAD;
    laser_scan.range_min = 0.5;
    laser_scan.range_max = 3.0;
    laser_scan.angle_min = -HORIZONTAL_VIEW_RAD/2;
    laser_scan.angle_max =  HORIZONTAL_VIEW_RAD/2;
    for (int i=0; i<LASER_SCANNER_POINTS; i++)
        laser_scan.ranges.push_back(laser_scan.range_max);

    //==================================================================
    //=========================== TRANSFORM ============================
    //==================================================================

    tf::TransformListener listener[2];
    tf::StampedTransform transform[2];

    ROS_INFO("Waiting for transforms...");
    if (sensor1)
    {
        try
        {
            listener[0].waitForTransform(base_link_frame, camera_left_link_frame,
                                         ros::Time(0), ros::Duration(1.0));
            listener[0].lookupTransform(base_link_frame, camera_left_link_frame,
                                        ros::Time(0), transform[0]);
        } catch (tf::TransformException ex)
        {
            ROS_ERROR("%s",ex.what());
            ros::Duration(1.0).sleep();
            std::exit(-1);
        }

        tf::Vector3 vec = transform[0].getOrigin();
        tf::Matrix3x3 rot = transform[0].getBasis();
        tfScalar rot_z, rot_y, rot_x;
        rot.getEulerYPR(rot_z, rot_y, rot_x);
        transformation_matrix1.setRotationYPR(vec[0], vec[1], vec[2],
                                              rot_z, rot_y, rot_x);
    }

    if (sensor2)
    {
        try
        {
            listener[1].waitForTransform(base_link_frame, camera_right_link_frame,
                                         ros::Time(0), ros::Duration(1.0));
            listener[1].lookupTransform(base_link_frame, camera_right_link_frame,
                                        ros::Time(0), transform[1]);
        } catch (tf::TransformException ex)
        {
            ROS_ERROR("%s",ex.what());
            ros::Duration(1.0).sleep();
            std::exit(-1);
        }

        tf::Vector3 vec = transform[1].getOrigin();
        tf::Matrix3x3 rot = transform[1].getBasis();
        tfScalar rot_z, rot_y, rot_x;
        rot.getEulerYPR(rot_z, rot_y, rot_x);
        transformation_matrix2.setRotationYPR(vec[0], vec[1], vec[2],
                                              rot_z, rot_y, rot_x);
    }
    ROS_INFO("Transforms obtained successfully");

    //==================================================================
    //=============================== Debug ============================
    //==================================================================

    n.getParam("debug", debug);

    if (debug)
    {
        std::vector<sensor_msgs::PointField> point_fields;
        sensor_msgs::PointField point_field;
        point_field.name = "x";
        point_field.offset = 0;
        point_field.datatype = 7;
        point_field.count = 1;
        point_fields.push_back(point_field);
        point_field.name = "y";
        point_field.offset = 4;
        point_fields.push_back(point_field);
        point_field.name = "z";
        point_field.offset = 8;
        point_fields.push_back(point_field);

        pub_cloud_floor = n.advertise<sensor_msgs::PointCloud2>("rgbd2laser/cloud_floor", 1);
        pub_cloud_positive = n.advertise<sensor_msgs::PointCloud2>("rgbd2laser/cloud_positive", 1);
        pub_cloud_negative = n.advertise<sensor_msgs::PointCloud2>("rgbd2laser/cloud_negative", 1);

        cloud_floor = new sensor_msgs::PointCloud2;
        cloud_floor->header.frame_id = base_link_frame;
        cloud_floor->height = 0;
        cloud_floor->width = 0;
        cloud_floor->fields = point_fields;
        cloud_floor->point_step = 16;

        cloud_positive = new sensor_msgs::PointCloud2;
        cloud_positive->header.frame_id = base_link_frame;
        cloud_positive->height = 0;
        cloud_positive->width = 0;
        cloud_positive->fields = point_fields;
        cloud_positive->point_step = 16;

        cloud_negative = new sensor_msgs::PointCloud2;
        cloud_negative->header.frame_id = base_link_frame;
        cloud_negative->height = 0;
        cloud_negative->width = 0;
        cloud_negative->fields = point_fields;
        cloud_negative->point_step = 16;
    }
}
RGBD2Laser::~RGBD2Laser()
{
    if (debug)
    {
        delete cloud_floor;
        delete cloud_positive;
        delete cloud_negative;
    }

    delete gpu;
}

void RGBD2Laser::sensorCb(const sensor_msgs::PointCloud2ConstPtr &cloud_1,
                          const sensor_msgs::PointCloud2ConstPtr &cloud_2)
{
    if (debug)
    {
        int cloud_floor_points;
        unsigned char *cloud_floor_data;
        gpu->extractCloudFloor(&cloud_1->data[0],  transformation_matrix1.getTransformationMatrix(),
                               0.05, &cloud_floor_data, cloud_floor_points);
        cloud_floor->height = 1;
        cloud_floor->width = cloud_floor_points;
        cloud_floor->row_step = 16*cloud_floor_points;
        cloud_floor->data.resize(16*cloud_floor_points);
        std::memcpy(cloud_floor->data.data(), cloud_floor_data, 16*cloud_floor_points*sizeof(unsigned char));
        delete cloud_floor_data;
        cloud_floor->header.stamp = ros::Time::now();
        pub_cloud_floor.publish(*cloud_floor);

        int cloud_positive_points;
        unsigned char *cloud_positive_data;
        gpu->extractCloudObstacle(&cloud_1->data[0],  transformation_matrix1.getTransformationMatrix(),
                                  0.05, 0.3, &cloud_positive_data, cloud_positive_points);
        cloud_positive->height = 1;
        cloud_positive->width = cloud_positive_points;
        cloud_positive->row_step = 16*cloud_positive_points;
        cloud_positive->data.resize(16*cloud_positive_points);
        std::memcpy(cloud_positive->data.data(), cloud_positive_data, 16*cloud_positive_points*sizeof(unsigned char));
        delete cloud_positive_data;
        cloud_positive->header.stamp = ros::Time::now();
        pub_cloud_positive.publish(*cloud_positive);

        int cloud_negative_points;
        unsigned char *cloud_negative_data;
        gpu->extractCloudObstacle(&cloud_1->data[0],  transformation_matrix1.getTransformationMatrix(),
                                  -4.0, -0.05, &cloud_negative_data, cloud_negative_points);
        cloud_negative->height = 1;
        cloud_negative->width = cloud_negative_points;
        cloud_negative->row_step = 16*cloud_negative_points;
        cloud_negative->data.resize(16*cloud_negative_points);
        std::memcpy(cloud_negative->data.data(), cloud_negative_data, 16*cloud_negative_points*sizeof(unsigned char));
        delete cloud_negative_data;
        cloud_negative->header.stamp = ros::Time::now();
        pub_cloud_negative.publish(*cloud_negative);
    }

    if (verbose)
    {
        std::cout << "========== SYNC===========" << std::endl;
        std::cout << "CLOUD1= " << cloud_1->header.stamp.sec << std::endl;
        std::cout << "CLOUD1= " << cloud_1->header.stamp.nsec/1000000 << std::endl;
        std::cout << "CLOUD2= " << cloud_2->header.stamp.sec << std::endl;
        std::cout << "CLOUD2= " << cloud_2->header.stamp.nsec/1000000 << std::endl;
        std::cout << "START = " << ros::Time::now().sec << std::endl;
        std::cout << "START = " << ros::Time::now().nsec/1000000 << std::endl;
    }

    for (int i=0; i<LASER_SCANNER_POINTS; i++)
        laser_scan.ranges[i] = laser_scan.range_max;
    gpu->computeLaserScan(&cloud_1->data[0], transformation_matrix1.getTransformationMatrix(),
                          &cloud_2->data[0], transformation_matrix2.getTransformationMatrix(),
                          0.075, 0.3, laser_scan.ranges.data());

    if (verbose)
    {
        std::cout << "STOP  = " << ros::Time::now().sec << std::endl;
        std::cout << "STOP  = " << ros::Time::now().nsec/1000000 << std::endl;
        std::cout << "============" << std::endl;
    }

    laser_scan.header.stamp = cloud_1->header.stamp + (cloud_2->header.stamp - cloud_1->header.stamp) * 0.5;
    pub_laser_scan.publish(laser_scan);
}

void RGBD2Laser::processCloud1(const sensor_msgs::PointCloud2ConstPtr &cloud)
{
    cloud_1 = cloud;
}
void RGBD2Laser::processCloud2(const sensor_msgs::PointCloud2ConstPtr &cloud)
{
    cloud_2 = cloud;
    publish();
}
void RGBD2Laser::publish()
{
    if (verbose)
    {
        std::cout << "========== SYNC===========" << std::endl;
        std::cout << "CLOUD1= " << cloud_1->header.stamp.sec << std::endl;
        std::cout << "CLOUD1= " << cloud_1->header.stamp.nsec/1000000 << std::endl;
        std::cout << "CLOUD2= " << cloud_2->header.stamp.sec << std::endl;
        std::cout << "CLOUD2= " << cloud_2->header.stamp.nsec/1000000 << std::endl;
        std::cout << "START = " << ros::Time::now().sec << std::endl;
        std::cout << "START = " << ros::Time::now().nsec/1000000 << std::endl;
    }

    for (int i=0; i<LASER_SCANNER_POINTS; i++)
        laser_scan.ranges[i] = laser_scan.range_max;
    gpu->computeLaserScan(&cloud_1->data[0], transformation_matrix1.getTransformationMatrix(),
                          &cloud_2->data[0], transformation_matrix2.getTransformationMatrix(),
                          0.075, 0.3, laser_scan.ranges.data());

    if (verbose)
    {
        std::cout << "STOP  = " << ros::Time::now().sec << std::endl;
        std::cout << "STOP  = " << ros::Time::now().nsec/1000000 << std::endl;
        std::cout << "============" << std::endl;
    }

    laser_scan.header.stamp = cloud_1->header.stamp + (cloud_2->header.stamp - cloud_1->header.stamp) * 0.5;
    pub_laser_scan.publish(laser_scan);
}
