// header
#include <camera_calibration.h>

// std
#include <algorithm>

// ros
#include <tf/transform_listener.h>
#include <tf_conversions/tf_eigen.h>
#include <tf/transform_broadcaster.h>
#include <pcl_conversions/pcl_conversions.h>

// pcl
#include <pcl/common/transforms.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/segmentation/sac_segmentation.h>

CameraCalibration::CameraCalibration(ros::NodeHandle& n)
{
    //==================================================================
    //============================ ROS PARAMS ==========================
    //==================================================================

    state = 0;
    n.getParam("robot_name", robot_name);
    n.getParam("sensor_name", sensor_name);

    if (robot_name.empty() || sensor_name.empty())
    {
        cerr << "You should specify robot_name and sensor_name params!" << endl;
        exit(-1);
    }

    //==================================================================
    //================================ TF ==============================
    //==================================================================

    tf::TransformListener tl;
    tf::StampedTransform st;
    while(ros::ok())
    {
        try
        {
            tl.waitForTransform(robot_name + "/base_link", robot_name + "/base_laser_link", ros::Time(0), ros::Duration(1.0));
            tl.lookupTransform(robot_name + "/base_link", robot_name + "/base_laser_link", ros::Time(0), st);
            tf::transformTFToEigen(st, laser_to_base_matrix);
        } catch (tf::TransformException ex)
        {
            cout << "Waiting for tf data.."  << endl;
            continue;
        }

        cout << "I've got tf data!" << endl;
        break;
    }

    //==================================================================
    //========================= Sub & Publishers =======================
    //==================================================================

    sub_cloud = n.subscribe<sensor_msgs::PointCloud2>("/" + robot_name + "/" + sensor_name + "/depth/points", 1, &CameraCalibration::processCloud, this);
    sub_laser_scan = n.subscribe<sensor_msgs::LaserScan>("/" + robot_name + "/scan", 1, &CameraCalibration::processLaserScan, this);

    //==================================================================
    //=========================== Visualizer ===========================
    //==================================================================

    viewer_cloud = PointCloudXYZRGB::Ptr(new PointCloudXYZRGB);
    viewer_inliers = pcl::PointIndicesPtr(new pcl::PointIndices);
    viewer_coeff = pcl::ModelCoefficientsPtr(new pcl::ModelCoefficients);

    viewer = VisualizerPtr(new Visualizer("3D Viewer"));
    viewer->setBackgroundColor(0, 0, 0);
    viewer->addCoordinateSystem(1.0);
    viewer->initCameraParameters();
    viewer->registerKeyboardCallback(&CameraCalibration::keyboardViewerEvent, *this, (void*)&viewer);
    pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGB>rgb(viewer_cloud);
    viewer->addPointCloud<pcl::PointXYZRGB>(viewer_cloud, rgb, "viewer_cloud");
    viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 3, "viewer_cloud");

    laser_index_min = 0;
    laser_index_max = 0;

    sensor_x = sensor_y = sensor_z = 0.0;
    sensor_pitch = sensor_yaw = 0.0;

    state = 1;
}

void CameraCalibration::publish()
{
    static tf::TransformBroadcaster br;
    tf::Transform transform;
    transform.setOrigin(tf::Vector3(sensor_x, sensor_y, sensor_z));
    tf::Quaternion q;
    q.setRPY(0.0, sensor_pitch, sensor_yaw);
    transform.setRotation(q);
    br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "/" + robot_name + "/base_link", "/" + robot_name + "/" + sensor_name + "_link"));
}

void CameraCalibration::processCloud(const sensor_msgs::PointCloud2ConstPtr &cloud)
{
    if (state == 1) determinePitchAngle(cloud);
    if (state == 3 || state == 4 || state == 6) determineYawXYAngle(cloud);
    //if (state == 7) visualize(cloud);
}
void CameraCalibration::processLaserScan(const sensor_msgs::LaserScanConstPtr &scan)
{
    if (state == 2 || state == 5) findWallOnLaserScan(scan);
    if (state  == 7) saveTransform();
    //if (state == 8) visualize(scan);
}

void CameraCalibration::keyboardViewerEvent(const pcl::visualization::KeyboardEvent &event, void *viewer_void)
{
    VisualizerPtr viewer = *static_cast<VisualizerPtr*>(viewer_void);

    // CTRL + C
    if (event.getKeyCode() == 3 && event.keyDown() && event.isCtrlPressed())
    {
        cout << "END IT!" << endl;
        exit(0);
    }

    // Laser scan manipulation
    if (state == 2 || state == 5)
    {
        if (event.getKeyCode() == 'v' && event.keyDown()) --laser_index_min;
        if (event.getKeyCode() == 'b' && event.keyDown()) ++laser_index_min;
        if (event.getKeyCode() == 'n' && event.keyDown()) --laser_index_max;
        if (event.getKeyCode() == 'm' && event.keyDown()) ++laser_index_max;
        if (event.getKeyCode() == 13 && event.keyDown()) viewer->close();
    }

    // Next segment
    if (state == 1 || state == 3 || state == 4 || state == 6)
    {
        // Next segment
        if (event.getKeyCode() == 27  && event.keyDown())
        {
            removeBiggestPlane();
            findBiggestPlane();
            viewer->updatePointCloud(viewer_cloud, "viewer_cloud");
        }
    }

    // Pitch angle
    if (state == 1)
    {
        // Accept it
        if (event.getKeyCode() == 13 && event.keyDown())
        {
            if (viewer_coeff->values[2]<0)
            {
                viewer_coeff->values[0] *= -1;
                viewer_coeff->values[1] *= -1;
                viewer_coeff->values[2] *= -1;
                viewer_coeff->values[3] *= -1;
            }
            sensor_pitch = M_PI_2 - atan2(viewer_coeff->values[1], viewer_coeff->values[2]);
            sensor_z = -viewer_coeff->values[3];
            cout << "pitch   = " << sensor_pitch << endl;
            cout << "z       = " << sensor_z << endl;

            viewer->close();
        }
    }

    // Yaw angle
    if (state == 3)
    {
        // Accept it
        if (event.getKeyCode() == 13 && event.keyDown())
        {
            if (viewer_coeff->values[0]<0)
            {
                viewer_coeff->values[0] *= -1;
                viewer_coeff->values[1] *= -1;
                viewer_coeff->values[2] *= -1;
                viewer_coeff->values[3] *= -1;
            }

            float cloud_a_1 = -viewer_coeff->values[1]/viewer_coeff->values[0];
            cout << "cloud_a_1 = " << cloud_a_1 << endl;
            sensor_yaw = atan(cloud_a_1) - atan(laser_a_1);
            cout << "Yaw       = " << sensor_yaw << endl;

            viewer->close();
        }
    }

    // Cloud_1 coeff
    if (state == 4)
    {
        // Accept it
        if (event.getKeyCode() == 13 && event.keyDown())
        {
            if (viewer_coeff->values[0]<0)
            {
                viewer_coeff->values[0] *= -1;
                viewer_coeff->values[1] *= -1;
                viewer_coeff->values[2] *= -1;
                viewer_coeff->values[3] *= -1;
            }

            cloud_a_1 = -viewer_coeff->values[1]/viewer_coeff->values[0];
            cloud_b_1 = -viewer_coeff->values[3]/viewer_coeff->values[0];
            cout << "cloud_a_1 = " << cloud_a_1 << endl;
            cout << "cloud_b_1 = " << cloud_b_1 << endl;
            sensor_x = laser_b_1 - cloud_b_1;
            cout << "X       = " << sensor_x << endl;

            viewer->close();
        }
    }

    // XY
    if (state == 6)
    {
        // Accept it
        if (event.getKeyCode() == 13 && event.keyDown())
        {
            if (viewer_coeff->values[0]<0)
            {
                viewer_coeff->values[0] *= -1;
                viewer_coeff->values[1] *= -1;
                viewer_coeff->values[2] *= -1;
                viewer_coeff->values[3] *= -1;
            }

            // second wall coefficients
            cloud_a_2 = -viewer_coeff->values[1]/viewer_coeff->values[0];
            cloud_b_2 = -viewer_coeff->values[3]/viewer_coeff->values[0];
            cout << "cloud_a_2 = " << cloud_a_2 << endl;
            cout << "cloud_b_2 = " << cloud_b_2 << endl;

            // translation between points
            float y1 = (cloud_b_2 - laser_b_1) / (laser_a_1 - cloud_a_2);
            float x1 = y1 * laser_a_1 + laser_b_1;
            float y2 = (laser_b_2 - laser_b_1) / (laser_a_1 - laser_a_2);
            float x2 = y2 * laser_a_1 + laser_b_1;

            float d_x = x2 - x1;
            float d_y = y2 - y1;

            // translation of sensor
            sensor_x += d_x;
            sensor_y += d_y;

            cout << "X       = " << sensor_x << endl;
            cout << "Y       = " << sensor_y << endl;

            viewer->close();
        }
    }
}

void CameraCalibration::findWallOnLaserScan(const sensor_msgs::LaserScanConstPtr &scan)
{
    cout << "state = " << state << " laser!" << endl;

    //Initialize first angles
    laser_index_min = 1*scan->ranges.size()/4;
    laser_index_max = 3*scan->ranges.size()/4;
    int min_index = 0, max_index = 0;

    // Display loop
    while(ros::ok() && !viewer->wasStopped())
    {
        if (laser_index_min != min_index || laser_index_max != max_index)
        {
            viewer_cloud->clear();
            for (int i=laser_index_min; i<laser_index_max; i++)
            {
                //nan
                if (scan->ranges.at(i) != scan->ranges.at(i)) continue;

                //from polar to cart
                float b = scan->angle_min + scan->angle_increment*i;
                float r = scan->ranges.at(i);
                float x = r * cos(b);
                float y = r * sin(b);
                pcl::PointXYZRGB point(250, 120, 120);
                point.x = x;
                point.y = y;
                point.z = 0;
                point = pcl::transformPoint(point, (Eigen::Affine3f)laser_to_base_matrix);
                viewer_cloud->push_back(point);
            }

            viewer->updatePointCloud(viewer_cloud, "viewer_cloud");
            min_index = laser_index_min;
            max_index = laser_index_max;
        }

        viewer->spinOnce(100);
        boost::this_thread::sleep(boost::posix_time::microseconds(100000));
    }
    viewer->resetStoppedFlag();

    // Fit linear
    PointCloudXYZ laser_cloud;
    for (int i=min_index; i<max_index; i++)
    {
        //nan
        if (scan->ranges.at(i) != scan->ranges.at(i)) continue;

        //from polar to cart
        float b = scan->angle_min + scan->angle_increment*i;
        float r = scan->ranges.at(i);
        float x = r * cos(b);
        float y = r * sin(b);
        pcl::PointXYZ point(x,y,0);
        point = pcl::transformPoint(point, (Eigen::Affine3f)laser_to_base_matrix);
        laser_cloud.push_back(point);
    }

    // Linear regression
    float inner = 0.0;
    float x_avg = 0.0;
    float y_avg = 0.0;
    float y_2 = 0.0;
    for (int i=0; i<laser_cloud.size(); i++)
    {
        x_avg   += laser_cloud.at(i).x;
        y_2     += laser_cloud.at(i).y * laser_cloud.at(i).y;
        y_avg   += laser_cloud.at(i).y;
        inner   += laser_cloud.at(i).x * laser_cloud.at(i).y;
    }
    x_avg /= laser_cloud.size();
    y_avg /= laser_cloud.size();
    if (state == 2)
    {
        laser_a_1 = (inner - laser_cloud.size() * x_avg * y_avg) / (y_2 - laser_cloud.size() * y_avg * y_avg);
        laser_b_1 = x_avg - laser_a_1*y_avg;
        cout << "laser_a_1 = " << laser_a_1 << endl;
        cout << "laser_b_1 = " << laser_b_1 << endl;
    } else if (state == 5)
    {
        laser_a_2 = (inner - laser_cloud.size() * x_avg * y_avg) / (y_2 - laser_cloud.size() * y_avg * y_avg);
        laser_b_2 = x_avg - laser_a_2*y_avg;
        cout << "laser_a_2 = " << laser_a_2 << endl;
        cout << "laser_b_2 = " << laser_b_2 << endl;
    }

    ++state;
    cout << "state = " << state << " laserend!" << endl;
}

void CameraCalibration::determinePitchAngle(const sensor_msgs::PointCloud2ConstPtr &cloud)
{
    // Convert to PCL
    PointCloudXYZ::Ptr cloud_pcl(new PointCloudXYZ);
    pcl::PCLPointCloud2 cloud_ros;
    pcl_conversions::toPCL(*cloud, cloud_ros);
    pcl::fromPCLPointCloud2(cloud_ros, *cloud_pcl);
    copyPointCloud(*cloud_pcl, *viewer_cloud);
    findBiggestPlane();
    viewer->updatePointCloud(viewer_cloud, "viewer_cloud");

    // Display loop
    while(ros::ok() && !viewer->wasStopped())
    {
        viewer->spinOnce(100);
        boost::this_thread::sleep(boost::posix_time::microseconds(100000));
    }
    viewer->resetStoppedFlag();

    ++state;
}
void CameraCalibration::determineYawXYAngle(const sensor_msgs::PointCloud2ConstPtr &cloud)
{
    cout << "state = " << state << " yaw!" << endl;

    // Get actual transformation
    tf::Transform transform_pitch, transform_base;
    transform_pitch.setOrigin(tf::Vector3(sensor_x, sensor_y, sensor_z));
    tf::Quaternion q1, q2;
    q1.setRPY(0.0, sensor_pitch, sensor_yaw);
    transform_pitch.setRotation(q1);
    q2.setRPY(-M_PI_2, 0.0, -M_PI_2);
    transform_base.setRotation(q2);

    // Cast it to eigen matrix
    tf::Transform final_transform = transform_pitch*transform_base;
    Eigen::Affine3d eigen = Eigen::Affine3d::Identity();
    tf::transformTFToEigen(final_transform, eigen);

    // Convert to PCL
    PointCloudXYZ::Ptr cloud_pcl(new PointCloudXYZ);
    pcl::PCLPointCloud2 cloud_ros;
    pcl_conversions::toPCL(*cloud, cloud_ros);
    pcl::fromPCLPointCloud2(cloud_ros, *cloud_pcl);
    pcl::transformPointCloud(*cloud_pcl, *cloud_pcl, eigen);
    copyPointCloud(*cloud_pcl, *viewer_cloud);
    findBiggestPlane();
    viewer->updatePointCloud(viewer_cloud, "viewer_cloud");

    // Display loop
    while(ros::ok() && !viewer->wasStopped())
    {
        viewer->spinOnce(1000);
        boost::this_thread::sleep(boost::posix_time::microseconds(100000));
    }
    viewer->resetStoppedFlag();
    ++state;

    cout << "state = " << state << " yawend!" << endl;
}

void CameraCalibration::saveTransform()
{
    string base = string(PROJECT_SOURCE_DIR);
    base.erase(base.begin(), base.begin()+1);
    base.erase(base.end()-1, base.end());
    string file_name = base + "/config/" + sensor_name + ".yaml";
    ofstream file;
    file.open(file_name);

    file << "sensor_x: " << sensor_x << endl;
    file << "sensor_y: " << sensor_y << endl;
    file << "sensor_z: " << sensor_z << endl;
    file << "sensor_roll: 0.0" << endl;
    file << "sensor_pitch: " << sensor_pitch << endl;
    file << "sensor_yaw: " << sensor_yaw << endl;
    file.close();
    ++state;
}

void CameraCalibration::findBiggestPlane()
{
    // Segmentation object
    pcl::SACSegmentation<pcl::PointXYZRGB> seg;
    seg.setModelType(pcl::SACMODEL_PLANE);
    seg.setMethodType(pcl::SAC_RANSAC);
    seg.setDistanceThreshold(0.02);
    seg.setOptimizeCoefficients(true);
    seg.setInputCloud(viewer_cloud);
    seg.segment(*viewer_inliers, *viewer_coeff);

    //color
    for (size_t i = 0; i < viewer_cloud->points.size(); ++i)
    {
        viewer_cloud->points[i].r = 180;
        viewer_cloud->points[i].g = 180;
        viewer_cloud->points[i].b = 250;
    }
    for (size_t i = 0; i < viewer_inliers->indices.size(); ++i)
    {
        viewer_cloud->points[viewer_inliers->indices.at(i)].r = 250;
        viewer_cloud->points[viewer_inliers->indices.at(i)].g = 180;
        viewer_cloud->points[viewer_inliers->indices.at(i)].b = 180;
    }
}
void CameraCalibration::removeBiggestPlane()
{
    pcl::ExtractIndices<pcl::PointXYZRGB> extract;
    extract.setNegative(true);
    extract.setInputCloud(viewer_cloud);
    extract.setIndices(viewer_inliers);
    extract.filter(*viewer_cloud);
}

void CameraCalibration::visualize(const sensor_msgs::LaserScanConstPtr &scan)
{
    // visualize laser scan
    for (int i=0; i<scan->ranges.size(); i++)
    {
        //nan
        if (scan->ranges.at(i) != scan->ranges.at(i)) continue;

        //from polar to cart
        float b = scan->angle_min + scan->angle_increment*i;
        float r = scan->ranges.at(i);
        float x = r * cos(b);
        float y = r * sin(b);
        pcl::PointXYZRGB point(120, 0, 0);
        point.x = x;
        point.y = y;
        point.z = 0;
        point = pcl::transformPoint(point, (Eigen::Affine3f)laser_to_base_matrix);
        viewer_cloud->push_back(point);
    }

    // visualize line
    for (float y=-1.0; y<1; y+=0.1)
    {
        pcl::PointXYZRGB point(250, 0, 120);
        point.x = laser_a_1*y + laser_b_1;
        point.y = y;
        point.z = 1;
        viewer_cloud->push_back(point);

        point.x = laser_a_2*y + laser_b_2;
        point.y = y;
        point.z = 0;
        viewer_cloud->push_back(point);
    }
    viewer->updatePointCloud(viewer_cloud, "viewer_cloud");


    // Display loop
    while(ros::ok() && !viewer->wasStopped())
    {
        viewer->spinOnce(1000);
        boost::this_thread::sleep(boost::posix_time::microseconds(100000));
    }
    viewer->resetStoppedFlag();

    // next state
    ++state;
}

void CameraCalibration::visualize(const sensor_msgs::PointCloud2ConstPtr &cloud)
{
    // clear if 7
    if (state == 7) viewer_cloud->clear();

    // Get actual transformation
    tf::Transform transform_pitch, transform_base;
    transform_pitch.setOrigin(tf::Vector3(sensor_x, sensor_y, sensor_z));
    tf::Quaternion q1, q2;
    q1.setRPY(0.0, sensor_pitch, sensor_yaw);
    transform_pitch.setRotation(q1);
    q2.setRPY(-M_PI_2, 0.0, -M_PI_2);
    transform_base.setRotation(q2);

    // Cast it to eigen matrix
    tf::Transform final_transform = transform_pitch*transform_base;
    Eigen::Affine3d eigen = Eigen::Affine3d::Identity();
    tf::transformTFToEigen(final_transform, eigen);

    // Convert to PCL
    PointCloudXYZ::Ptr cloud_pcl(new PointCloudXYZ);
    pcl::PCLPointCloud2 cloud_ros;
    pcl_conversions::toPCL(*cloud, cloud_ros);
    pcl::fromPCLPointCloud2(cloud_ros, *cloud_pcl);
    pcl::transformPointCloud(*cloud_pcl, *cloud_pcl, eigen);
    copyPointCloud(*cloud_pcl, *viewer_cloud);

    for (int i=0; i<viewer_cloud->size(); i++)
    {
        viewer_cloud->points[i].r = 180;
        viewer_cloud->points[i].g = 180;
        viewer_cloud->points[i].b = 250;
    }


    // visualize line
    for (float y=-1.0; y<1; y+=0.1)
    {
        pcl::PointXYZRGB point(120, 250, 120);
        point.x = cloud_a_1*y + cloud_b_1;
        point.y = y;
        point.z = 1;
        viewer_cloud->push_back(point);

        point.x = cloud_a_2*y + cloud_b_2;
        point.y = y;
        point.z = 0;
        viewer_cloud->push_back(point);
    }
    viewer->updatePointCloud(viewer_cloud, "viewer_cloud");

    // next state
    ++state;
}
