// ROS includes
#include <ros/ros.h>
#include <tf/transform_broadcaster.h>

// Main
int main(int argc, char** argv)
{
    //Init ros node ======================================================
    ros::init(argc, argv, "static_transform_publisher");
    ros::NodeHandle n("~");
    ros::Rate rate(30);

    std::string robot_name, sensor_name;
    float sensor_x, sensor_y, sensor_z;
    float sensor_roll, sensor_pitch, sensor_yaw;

    n.getParam("robot_name", robot_name);
    n.getParam("sensor_name", sensor_name);

    n.getParam("sensor_x", sensor_x);
    n.getParam("sensor_y", sensor_y);
    n.getParam("sensor_z", sensor_z);

    n.getParam("sensor_roll", sensor_roll);
    n.getParam("sensor_pitch", sensor_pitch);
    n.getParam("sensor_yaw", sensor_yaw);

    tf::TransformBroadcaster br;
    tf::Transform transform;
    transform.setOrigin(tf::Vector3(sensor_x, sensor_y, sensor_z));
    tf::Quaternion q;
    q.setRPY(sensor_roll, sensor_pitch, sensor_yaw);
    transform.setRotation(q);

    std::string robot_frame = "/" + robot_name + "/base_link";
    std::string sensor_frame= "/" + robot_name + "/" + sensor_name + "_link";

    //Loop
    while(ros::ok())
    {
        // Send transform
        br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), robot_frame, sensor_frame));

        // Sleep
        rate.sleep();
    }

    return 0;
}
