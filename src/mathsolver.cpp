#include <mathsolver.h>

void MathSolver::transformToPointCS(vector<Point3f> points, vector<Point3f> &newPoints)
{
    Point3f invertCoords(-1.0f, -1.0f, -1.0f);
    newPoints.clear();

    for(int i = 0; i < points.size(); i++)
    {
        newPoints.push_back(
                    Point3f(
                        points.at(i).x * invertCoords.x,
                        points.at(i).y * invertCoords.y,
                        points.at(i).z * invertCoords.z));
    }
}

void MathSolver::calculatePlane(vector<Point3f> points, Vec4f &coefficients)
{
    float A, B, C, D;

    Vec3f vec1(
                points.at(0).x - points.at(1).x,
                points.at(0).y - points.at(1).y,
                points.at(0).z - points.at(1).z);
    Vec3f vec2(
                points.at(1).x - points.at(2).x,
                points.at(1).y - points.at(2).y,
                points.at(1).z - points.at(2).z);
    Vec3f crossVec = vec1.cross(vec2);

    A = crossVec.val[0];
    B = crossVec.val[1];
    C = crossVec.val[2];
    D = A * points.at(0).x + B * points.at(0).y + C * points.at(0).z;

    coefficients.val[0] = A;
    coefficients.val[1] = B;
    coefficients.val[2] = C;
    coefficients.val[3] = D;
}

void MathSolver::calculateCircleCenter(vector<Point3f> points, Point3f &center)
{
    float y1 = points.at(0).z;
    float y2 = points.at(1).z;
    float y3 = points.at(2).z;

    float x1 = points.at(0).x;
    float x2 = points.at(1).x;
    float x3 = points.at(2).x;

    float m12 = (y2 - y1) / (x2 - x1);
    float m23 = (y3 - y2) / (x3 - x2);

    float x = (m12 * m23 * (y1 - y3) + m23 * (x1 + x2) - m12 * (x2 + x3)) / (2 * (m23 - m12));
    float y = - 1 / m12 * (x - (x1 + x2) / (2)) + (y1 + y2) / 2;

    center.x = x;
    center.y = 0.0f;
    center.z = y;
}

float MathSolver::calculateDistance(Point3f point, Point3f center)
{
    return sqrt(pow(point.x - center.x, 2) + pow(point.z - center.z, 2));
}

void MathSolver::getTranslationVector(Point3f from, Point3f to, Vec3f &translation)
{
    translation.val[0] = to.x - from.x;
    translation.val[1] = to.y - from.y;
    translation.val[2] = to.z - from.z;
}

void MathSolver::calculateTranslation(vector<vector<Point3f> > points, float &radius, Vec3f &translation, float range)
{
    vector<Point3f> tempPoints;
    vector<Point3f> circleCenters;
    vector<Point3f> circlePoints;
    vector<int> circlesInRange;
    Point3f tempCenter;
    Vec3f tempTranslation;

    float tempRadius = 0.0f;
    int index;
    int n = 0;

    for (int i = 0; i < points.size(); i++)
    {
        //NaN
        if ((points.at(i).at(0) != points.at(i).at(0)) ||
                (points.at(i).at(1) != points.at(i).at(1)) ||
                (points.at(i).at(2) != points.at(i).at(2)))
            continue;

        transformToPointCS(points.at(i), tempPoints);

        calculateCircleCenter(tempPoints, tempCenter);

        circlePoints.push_back(tempPoints.at(2));
        circleCenters.push_back(tempCenter);
    }

    for (int i = 0; i < circleCenters.size(); i++)
        circlesInRange.push_back(findCentersInRange(circleCenters, circleCenters.at(i), range));

    index = findIndexOfMax(circlesInRange);

    for (int i = 0; i < circleCenters.size(); i++)
    {
        if (calculateDistance(circleCenters.at(i), circleCenters.at(index)) < range)
        {
            printf("\tcircle center ->\tc: [%5.3f,%5.3f]\n", circleCenters.at(i).x, circleCenters.at(i).z);
            radius += calculateDistance(circlePoints.at(i), circleCenters.at(i));

            getTranslationVector(circleCenters.at(i), circlePoints.at(i), tempTranslation);
            translation += tempTranslation;

            n++;
        }
    }

    if (n != 0)
    {
        radius /= n;

        translation.val[0] /= n;
        translation.val[1] /= n;
        translation.val[2] /= n;
    }
}

void MathSolver::get3DPointFromDepthImage(Mat image, sensor_msgs::CameraInfo cameraInfo, Point2d imagePoint, Point3f &point)
{
    point.x = image.at<float>(imagePoint) * (imagePoint.x - cameraInfo.P[CX]) /  cameraInfo.P[FX];
    point.y = image.at<float>(imagePoint) * (imagePoint.y - cameraInfo.P[CY]) /  cameraInfo.P[FY];
    point.z = image.at<float>(imagePoint);
}

void MathSolver::calculateYawAngle(vector<vector<Point3f> > points, Vec3f translation, float &yaw)
{
    Point3f tempPoint;
    float tempYaw = 0.0f;
    vector<Point3f> tempPoints;
    int n = 0;

    for (int i = 0; i < points.size(); i++)
    {
        //NaN
        if ((points.at(i).at(0) != points.at(i).at(0)) ||
                (points.at(i).at(1) != points.at(i).at(1)))
            continue;

        transformToPointCS(points.at(i), tempPoints);

        tempPoint.x = points.at(i).at(0).x + translation.val[0];
        tempPoint.z = points.at(i).at(0).z + translation.val[2];

        calculateAngle(points.at(i).at(0), points.at(i).at(1), points.at(i).at(0), tempPoint, tempYaw);
        yaw += tempYaw;

        n++;
    }

    if (n != 0)
        yaw /= n;
}

void MathSolver::calculateAngle(Point3f a1, Point3f a2, Point3f b1, Point3f b2, float angle)
{
    Vec3f a, b;

    a.val[0] = a2.x - a1.x;
    a.val[1] = a2.y - a1.y;
    a.val[2] = a2.z - a1.z;

    b.val[0] = b2.x - b1.x;
    b.val[1] = b2.y - b1.y;
    b.val[2] = b2.z - b1.z;

    angle = acos(
                (a.val[0] * b.val[0] + a.val[2] * b.val[2]) /
                (sqrt(pow(a.val[0], 2) + pow(a.val[2], 2)) * sqrt(pow(b.val[0], 2) + pow(b.val[2], 2))));
}

int MathSolver::findCentersInRange(vector<Point3f> points, Point3f point, float range)
{
    int n = 0;

    for (int i = 0; i < points.size(); i++)
    {
        if (calculateDistance(points.at(i), point) < range)
            n++;
    }

    return n;
}

int MathSolver::findIndexOfMax(vector<int> numbers)
{
    int index = 0;

    for (int i = 1; i < numbers.size(); i++)
    {
        if (numbers.at(i) > numbers.at(index))
            index = i;
    }

    return index;
}

const int MathSolver::FX = 0;
const int MathSolver::FY = 5;
const int MathSolver::CX = 2;
const int MathSolver::CY = 6;
