// std
#include <string>

// ros
#include <ros/ros.h>
#include <geometry_msgs/Twist.h>
#include <tf/transform_listener.h>

// Main
int main(int argc, char** argv)
{
    //==================================================================
    //========================= INIT NODE ==============================
    //==================================================================

    ros::init(argc, argv, "odom_calib_trans");
    ros::NodeHandle n("~");

    //==================================================================
    //========================= ROS PARAMS =============================
    //==================================================================

    std::string odom_frame, base_link_frame;
    n.param<std::string>("odom_frame", odom_frame, "/batman/odom");
    n.param<std::string>("base_link_frame", base_link_frame, "/batman/base_link");

    //==================================================================
    //======================= START TRANSFORM ==========================
    //==================================================================

    tf::TransformListener listener;
    tf::StampedTransform transform;

    ROS_INFO("Waiting for start transform...");
    try
    {
        listener.waitForTransform(odom_frame, base_link_frame,
                                  ros::Time(0), ros::Duration(1.0));
        listener.lookupTransform(odom_frame, base_link_frame,
                                 ros::Time(0), transform);
    } catch (tf::TransformException ex)
    {
        ROS_ERROR("%s",ex.what());
        ros::Duration(1.0).sleep();
        std::exit(-1);
    }

    tfScalar start_yaw, start_pitch, start_roll;
    transform.getBasis().getEulerYPR(start_yaw, start_pitch, start_roll);
    tf::Vector3 start_vec = transform.getOrigin();
    ROS_INFO("Got start transform, go go go!");

    //==================================================================
    //========================== ROS SPIN ==============================
    //==================================================================

    ros::spin();

    //==================================================================
    //======================= STOP TRANSFORM ===========================
    //==================================================================

    ROS_INFO("Waiting for stop transform...");
    try
    {
        listener.waitForTransform(odom_frame, base_link_frame,
                                  ros::Time(0), ros::Duration(1.0));
        listener.lookupTransform(odom_frame, base_link_frame,
                                 ros::Time(0), transform);
    } catch (tf::TransformException ex)
    {
        ROS_ERROR("%s",ex.what());
        ros::Duration(1.0).sleep();
        std::exit(-1);
    }

    tfScalar stop_yaw, stop_pitch, stop_roll;
    transform.getBasis().getEulerYPR(stop_yaw, stop_pitch, stop_roll);
    tf::Vector3 stop_vec = transform.getOrigin();
    std::cout << "DISTANCE = " << stop_vec.distance(start_vec) << std::endl;
    std::cout << "ANGLE    = " << stop_yaw - start_yaw << std::endl;

    return 0;
}
