/**
  * Do zrobienia:
  * 1) Ładne zakończenie programu
  * 2) Dokładne zatrzymanie po określonym kącie
  * 3) Nagranie rosbaga
  * 4) Puszczenie rosbaga i sprawdzenie czy dziala program
  */

// STD includes
#include <string>

// ROS includes
#include <ros/ros.h>
#include <tf/transform_listener.h>
#include <tf_conversions/tf_eigen.h>
#include <geometry_msgs/Twist.h>
#include <sensor_msgs/PointCloud2.h>

// Calibration
#include <camera_calibration.h>
#include <rotation_based_calibration.h>

#include <message_filters/subscriber.h>
#include <message_filters/time_synchronizer.h>
#include <message_filters/sync_policies/exact_time.h>
#include <message_filters/sync_policies/approximate_time.h>
#include <sensor_msgs/Image.h>
#include <cv_bridge/cv_bridge.h>

#include <opencv2/highgui/highgui.hpp>

// PCL
#include <pcl/PCLPointCloud2.h>
#include <pcl/conversions.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/segmentation/sac_segmentation.h>

// Namespace
using namespace std;

class CalibrationObserver
{

public:

    //============================================================================
    //=============================== TYPEDEFS ===================================
    //============================================================================

    /** \brief Message filters with exact time policy. */
    typedef message_filters::sync_policies::ApproximateTime<sensor_msgs::Image,
                                                            sensor_msgs::Image> MySyncPolicy;

    /** \brief Synchronizer typedef. */
    typedef message_filters::Synchronizer<MySyncPolicy> MySynchronizer;

    /** \brief Subscriber image typedef. */
    typedef message_filters::Subscriber<sensor_msgs::Image> SubImage;

    //============================================================================
    //============================= MAIN METHODS =================================
    //============================================================================

    /**
     * @brief CalibrationObserver                       The class constructor.
     * @param n                                         Ros node handle object.
     * @param topic_image_color                         The topic of color image of the sensor.
     * @param topic_image_depth                         The topic of registered depth image of the sensor.
     * @param frame_base_link                           The base link frame of the robot.
     * @param frame_odom                                The odom frame of the robor.
     */
    CalibrationObserver(ros::NodeHandle& n,
                        const string &topic_image_color, const string &topic_image_depth,
                        const string &frame_base_link, const string &frame_odom)
    {
        // Subscribers
        sub_sensor_color = new SubImage(n, topic_image_color, 1);
        sub_sensor_depth = new SubImage(n, topic_image_depth, 1);
cout << topic_image_color << endl;
cout << topic_image_depth << endl;

        // Time sync
        sync = new MySynchronizer(MySyncPolicy(10), *sub_sensor_color, *sub_sensor_depth);
        sync->registerCallback(boost::bind(&CalibrationObserver::sensors_callback, this, _1, _2));

        // Frames
        this->frame_base_link = frame_base_link;
        this->frame_odom = frame_odom;

        // Working flag
        working = true;
    }

    /** \brief Returns true if the calibration isn't done yet. */
    bool isWorking()
    {
        return working;
    }

protected:

    //============================================================================
    //=============================== CONSTANTS ==================================
    //============================================================================

    /** \brief Minimum angle increment between saved frames in radians */
    #define MIN_ANGLE_INCREMENT     ((float)(10 * M_PI / 180))

    //============================================================================
    //=============================== VARIABLES ==================================
    //============================================================================

    /** \brief RGBD sensor color image subscriber. */
    SubImage *sub_sensor_color;

    /** \brief RGBD sensor depth image subscriber. */
    SubImage *sub_sensor_depth;

    /** \brief RGBD sensor messages synchronization. */
    MySynchronizer *sync;

    /** \brief Name of base link frame of the robot. */
    string frame_base_link;

    /** \brief Name of odom frame of the robot. */
    string frame_odom;

    /** \brief Last yaw angle between base_link and odom frames. */
    float yaw_last;

    /** \brief Starting yaw angle between base_link and odom frames. */
    float yaw_start;

    /** \brief Working flag */
    bool working;

    /** \brief Camera rotation based calibration var. */
    RotationBasedCalibration camera_rot_calib;

    //============================================================================
    //============================ SENSORS CALLBACK ==============================
    //============================================================================

    /** \brief Sensor cloud received callback */
    void sensors_callback(const sensor_msgs::ImageConstPtr &image_color,
                          const sensor_msgs::ImageConstPtr &image_depth)
    {
        // Info
        cout << "[CalibrationObserver] : sensor callback" << endl;

        // Vars
        tf::TransformListener listener;
        tf::StampedTransform transform;
        double roll, pitch, yaw;

        // Find transform
        try
        {
            ros::Time stamp = ros::Time(0);
            listener.waitForTransform(frame_odom, frame_base_link, stamp, ros::Duration(0.5));
            listener.lookupTransform(frame_base_link, frame_odom, stamp, transform);
        }
        catch(tf::TransformException ex)
        {
            cerr << "[CalibrationObserver] : " << ex.what() << endl;
            ros::Duration(0.1).sleep();
            return;
        }

        // Get angles
        tf::Quaternion quat = transform.getRotation();
        tf::Matrix3x3(quat).getRPY(roll, pitch, yaw);

        // First frame or it's time to add a new frame
        if (!camera_rot_calib.getFramesCount() || abs(yaw - yaw_last) >= MIN_ANGLE_INCREMENT)
        {
            if (!camera_rot_calib.getFramesCount())
            {
                yaw_start = yaw;
                cout << "[CalibrationObserver] : start yaw angle = " << yaw << endl;
            }
            else cout << "[CalibrationObserver] : angle increase = " << yaw_last - yaw << endl;

            // Convert to opencv Mat
            cv_bridge::CvImageConstPtr image_color_temp, image_depth_temp;
            image_color_temp = cv_bridge::toCvCopy(image_color);
            image_depth_temp = cv_bridge::toCvCopy(image_depth);

            // Add frame
            camera_rot_calib.addFrame(image_color_temp->image,
                                      image_depth_temp->image,
                                      yaw);

            // Remember last yaw
            yaw_last = yaw;

            // If it's time to end this
            if (abs(yaw - yaw_start) >= (0.5*M_PI-MIN_ANGLE_INCREMENT/2))
            {
                camera_rot_calib.processRotation(); //(*)
                //MOVE FORWARD
                //->    camera_rot_calib.processMovement(); //(**)
                //->    if ((*) and (**))
                //->        camera_rot_calib.estimateTransformationMatrix(transformation_matrix);


    //            Eigen::Affine3d transformation_matrix;
    //            camera_rot_calib.estimateTransformationMatrix(transformation_matrix);
    //            if (!camera_rot_calib.estimateTransformationMatrix(transformation_matrix))
    //            {
    //                cerr << "SOME ERROR!" << endl;
    //            } else
    //            {
                    working = false;
                    cout << "SUCCESS" << endl;
    //            }
            }
        }
    }
};

class CalibrationRobotController
{

public:

    //============================================================================
    //=============================== TYPEDEFS ===================================
    //============================================================================

    /**
     * @brief The MoveMode enum                     Structure describing the move type.
     */
    enum MoveMode
    {
        ROTATE_90,
        MOVE_FORWARD
    };

    //============================================================================
    //============================= MAIN METHODS =================================
    //============================================================================

    /**
     * @brief CalibrationRobotController            The calibration robot controller class.
     * @param n                                     Ros nodehanle object.
     * @param topic_cmd_vel                         The cmd_vel topic of the robot.
     * @param mode                                  Mode of the controller.
     */\
    CalibrationRobotController(ros::NodeHandle& n, const string &topic_cmd_vel,
                               MoveMode mode)
    {
        // Publishers
        pub_cmd_vel = n.advertise<geometry_msgs::Twist>(topic_cmd_vel, 10);

        // Working flag
        this->mode = mode;
    }

    /**
     * @brief go_nonblocking                        Nonblocking go method, where user should take
     *                                              care when to stop the robot.
     * @param vel                                   Velocity of the move in the range <-1, +1>
     */
    void go_nonblocking(float vel)
    {
        if (mode == ROTATE_90)
        {
            geometry_msgs::Twist cmd_vel;
            cmd_vel.angular.z = vel;
            pub_cmd_vel.publish(cmd_vel);
        } else if (mode == MOVE_FORWARD)
        {
            geometry_msgs::Twist cmd_vel;
            cmd_vel.angular.x = vel;
            pub_cmd_vel.publish(cmd_vel);
        }
    }

    /**
     * @brief stop                                  Stop the robot.
     */
    void stop()
    {
        // Zero the cmd_vel
        geometry_msgs::Twist cmd_vel;
        pub_cmd_vel.publish(cmd_vel);
        ros::spinOnce();
        ros::Rate(0.5).sleep();
    }

protected:

    //============================================================================
    //=============================== VARIABLES ==================================
    //============================================================================

    /**
     * @brief mode                      Move mode.
     */
    MoveMode mode;

    /**
     * @brief pub_cmd_vel               cmd_vel publisher.
     */
    ros::Publisher pub_cmd_vel;
};

class ZPitchCalibration
{
public:

    //============================================================================
    //================================= TYPEDEFS =================================
    //============================================================================

    /**
     * @brief PointCloudXYZ     Pointcloud typedef.
     */
    typedef pcl::PointCloud<pcl::PointXYZ> PointCloudXYZ;


    //============================================================================
    //============================= MAIN METHODS =================================
    //============================================================================

    /** @brief Constructor. */
    ZPitchCalibration(ros::NodeHandle& n, const string &topic_cloud)
    {
        this->n = &n;
        this->topic_cloud = topic_cloud;
        blocked = false;
    }

    /**
     * @brief estimateZPitch                Estimate pitch angle and z coordinate
     *                                      of the sensor based on the floor view.
     * @param[in] cloud                     XYZ Cloud.
     * @param[out] z                        Output z coordinate of the sensor.
     * @param[out] pitch                    Output pitch angle of the sensor.
     * @return                              True if succeed.
     */
    bool estimateZPitchBlocking(float& z, float& pitch)
    {
        ros::Subscriber sub = n->subscribe<sensor_msgs::PointCloud2>(topic_cloud, 1, &ZPitchCalibration::pointcloud_callback, this);

        blocked = true;
        while (ros::ok() && blocked)
        {
            ros::spinOnce();
            ros::Rate(30).sleep();
        }

        z = this->z;
        pitch = this->pitch;
        return true;
    }

private:
    ros::NodeHandle *n;
    string topic_cloud;
    bool blocked;
    float z, pitch;

    void pointcloud_callback(const sensor_msgs::PointCloud2ConstPtr& cloud_msg)
    {
        // Conversion ROS->PCL
        pcl::PCLPointCloud2 pcl_pc2;
        pcl_conversions::toPCL(*cloud_msg,pcl_pc2);
        pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>);
        pcl::fromPCLPointCloud2(pcl_pc2,*cloud);

        // Vars
        pcl::PointIndices::Ptr inliers(new pcl::PointIndices);
        pcl::ModelCoefficients::Ptr coeff(new pcl::ModelCoefficients);

        // Find biggest plane
        pcl::SACSegmentation<pcl::PointXYZ> seg;
        seg.setModelType(pcl::SACMODEL_PLANE);
        seg.setMethodType(pcl::SAC_RANSAC);
        seg.setDistanceThreshold(0.02);
        seg.setOptimizeCoefficients(true);
        seg.setInputCloud(cloud);
        seg.segment(*inliers, *coeff);

        // Get params
        pitch = -M_PI_2 - atan2(coeff->values[1], coeff->values[2]);
        z = coeff->values[3];
//        cout << "PITCH = " << pitch << "\tZ = " << z << endl;
        blocked = false;
    }
};


// Main
int main(int argc, char** argv)
{
    //============================================================================
    //=============================== NODE INIT ==================================
    //============================================================================

    ros::init(argc, argv, "camera_rotation_calibration");
    ros::NodeHandle n("~");
    ros::Rate rate(30);

    //============================================================================
    //============================= PARAMS PARSING ===============================
    //============================================================================

    std::string ns = n.getNamespace();
    std::string parent_ns = ros::names::parentNamespace(ns);
    std::string robot_name, mode;
    if (!n.getParam("robot_name", robot_name))
    {
        cerr << n.getNamespace().c_str() << " : no param [robot_name] found" << endl;
        ros::Duration(0.1).sleep();
        return 0;
    }

    //============================================================================
    //================================== VARS ====================================
    //============================================================================

    // Vars
    float z, pitch;

    // Vars
    ZPitchCalibration *z_pitch_calib;
/*    CalibrationObserver *observer;
    CalibrationRobotController *controller;

    // Calibration observer
    observer = new CalibrationObserver(n,
                                       "/" + robot_name + "/camera/rgb/image_rect_color",
                                       "/" + robot_name + "/camera/depth_registered/hw_registered/image_rect",
                                       robot_name + "/base_link",
                                       robot_name + "/odom");
*/
    // ZPitchCalib
    z_pitch_calib = new ZPitchCalibration(n, "/" + robot_name + "/camera/depth_registered/points");

    //============================================================================
    //================================ 1ST STEP ==================================
    //============================================================================

    //Estimate zpitch
    z_pitch_calib->estimateZPitchBlocking(z, pitch);
    cout << "Z = " << z << " PITCH = " << pitch << endl;

    //============================================================================
    //================================ 2ND STEP ==================================
    //============================================================================
/*

    // Calibration robot controller
    controller = new CalibrationRobotController(n, "/" + robot_name + "/aria/cmd_vel",
                                                CalibrationRobotController::ROTATE_90);

    // GO!
    controller->go_nonblocking(0.1);

    //Loop
    while(ros::ok() && observer->isWorking())
    {
        //Spin
        ros::spinOnce();

        //Sleep
        rate.sleep();
    }

    // STOP
    controller->stop();
    delete controller;

    // PROCESS ROTATION
    cout << "TUTAJ BEDZIE PROCESSROTATION" << endl;

    //============================================================================
    //================================ 3RD STEP ==================================
    //============================================================================

    // Clean
    delete observer;
*/
    delete z_pitch_calib;

    // Return
    return 0;
}
