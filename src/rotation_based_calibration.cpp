// header
#include <rotation_based_calibration.h>

RotationBasedCalibration::RotationBasedCalibration()
{
    cameraInfo.P[MathSolver::FX] = 525.0f;
    cameraInfo.P[MathSolver::FY] = 525.0f;
    cameraInfo.P[MathSolver::CX] = 319.5f;
    cameraInfo.P[MathSolver::CY] = 239.5f;

    radius = 0.0f;
}

bool RotationBasedCalibration::addFrame(const cv::Mat &image_bgr, const cv::Mat &image_depth, const float yaw_angle)
{
    RGBDFrame frame = make_pair(image_bgr.clone(), image_depth.clone());
    frames.push_back(make_pair(frame, yaw_angle));
}

bool RotationBasedCalibration::estimateTransformationMatrix(Eigen::Affine3d &transformation_matrix)
{
    //complete transformation
}

bool RotationBasedCalibration::processRotation()
{
    //---------------------------------------------------------------------------------------------
    //-------------------- OPENCV 3.2 API ---------------------------------------------------------
    //---------------------------------------------------------------------------------------------
    //Ptr<ORB> orb = ORB::create();
    //Tracker tracker = Tracker(orb);
    //---------------------------------------------------------------------------------------------

    //OpenCV <= 3.0 API
    Tracker tracker = Tracker();
    vector<vector<Point3f> > points;

    //---------------------------------------------------------------------------------------------
    //--- !!! ALL COORDINATES ARE IN CAMERA COORDINATE SYSTEM !!! ---------------------------------
    //---------------------------------------------------------------------------------------------

    cout << "============================================================================" << endl;
    ROS_WARN("!!! ALL COORDINATES ARE IN CAMERA COORDINATE SYSTEM !!!");
    cout << "[RotationBasedCalibration] : STAGE I - Transformation Matrix estimation" << endl;

    cout << "[RotationBasedCalibration] : STEP 1 - Extracting keypoints..." << endl;
    for (int i = 0; i <= frames.size() - 3; i++)
    {
        cout << "[RotationBasedCalibration] : STEP 1 - Iteration:\t" << i + 1;
        tracker.setFirstFrame(frames.at(i).first.first);
        tracker.process(frames.at(i + 1).first.first);
        tracker.process(frames.at(i + 2).first.first);

        tracker.get3DPoints(frames.at(i).first.second, frames.at(i + 1).first.second, frames.at(i + 2).first.second, cameraInfo, points);
        cout << "... keypoints detected: " << points.size() << endl;
    }
    cout << "[RotationBasedCalibration] : STEP 1 - Keypoints extracted!" << endl;

    cout << "[RotationBasedCalibration] : STEP 2 - Calculating translation..." << endl;
    MathSolver::calculateTranslation(points, radius, translation);
    printf("\n\tr = %5.3f\n\ttranslation[x, _, z] = [%5.3f, _____, %5.3f]\n",
           radius,
           translation.val[0], translation.val[2]);
    cout << "[RotationBasedCalibration] : STEP 2 - Translation calculated!" << endl;
    cout << "[RotationBasedCalibration] : STAGE I - END" << endl;

    frames.clear();

    return true;
}

bool RotationBasedCalibration::processMovement()
{
    Ptr<ORB> orb = ORB::create();
    Tracker tracker = Tracker(orb);
    vector<vector<Point3f> > points;

    cout << "============================================================================" << endl;
    cout << "[RotationBasedCalibration] : STAGE II - Transformation Matrix estimation" << endl;

    cout << "[RotationBasedCalibration] : STEP 1 - Extracting keypoints..." << endl;
    for (int i = 0; i <= frames.size() - 2; i++)
    {
        cout << "[RotationBasedCalibration] : STEP 1 - Iteration:\t" << i + 1 << endl;
        tracker.setFirstFrame(frames.at(i).first.first);
        tracker.process(frames.at(i + 1).first.first);

        tracker.get3DPoints(frames.at(i).first.second, frames.at(i + 1).first.second, cameraInfo, points);

        for (int j = 0; j < points.size(); j++)
            printf("\t#%d:\t[%5.2f; %5.2f; %5.2f]\t[%5.2f; %5.2f; %5.2f]\n",
                     j,
                     points.at(j).at(0).x, points.at(j).at(0).y, points.at(j).at(0).z,
                     points.at(j).at(1).x, points.at(j).at(1).y, points.at(j).at(1).z);
    }
    cout << "[RotationBasedCalibration] : STEP 1 - Keypoints extracted!" << endl;

    cout << "[RotationBasedCalibration] : STEP 2 - Calculating YAW angle..." << endl;
    MathSolver::calculateYawAngle(points, translation, yaw);
    printf("\tYAW = %5.3f\n", yaw);
    cout << "[RotationBasedCalibration] : STEP 2 - YAW angle calculated!" << endl;
    cout << "[RotationBasedCalibration] : STAGE II - END" << endl;

    frames.clear();

    return true;
}

void RotationBasedCalibration::setCameraParameters(sensor_msgs::CameraInfo cameraParams)
{
    cameraInfo = cameraParams;
}
