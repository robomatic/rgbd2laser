// STD includes
#include <string>

// ROS includes
#include <ros/ros.h>

// Calibration
#include <camera_calibration.h>

// Namespace
using namespace std;

// Main
int main(int argc, char** argv)
{
    //Init ros node ======================================================
    ros::init(argc, argv, "camera_calibration");
    ros::NodeHandle n("~");
    ros::Rate rate(30);

    // Calibration
    CameraCalibration camera_calibration(n);

    //Loop
    while(ros::ok())
    {
        //Spin
        ros::spinOnce();

        //Publish
        camera_calibration.publish();

        //Sleep
        rate.sleep();
    }

    return 0;
}
